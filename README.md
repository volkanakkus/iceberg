# Iceberg Digital Front-End Case

Hey, It's Volkan. I built this case for Iceberg Digital.  

- Project framework: ```Nuxt.js - 2.15.8```
- Vuex: ```Dynamic Vuex Module Registration```
- CSS Framework: ```Tailwind CSS``` & ```PostCSS```
- Component Framework:  ```VueTailwind```
- HTTP Requests: ```Airtable``` & ```Axios```
- Proxy: ```@Nuxtjs/Proxy```
- Google Maps Wrapper: ```Vue2 Google Maps```
- Linting: ```Eslint``` & ```Prettier```
- External APIs: **```Google Maps Api```**  &  **```Postcodes.io```** 

I also designed whole UI by myself and chose Tailwind for reusability and easy implementation any design system to project. 
You can follow build introductions below to serve or build this project.

## Introductions

```bash
# to install dependencies
$ npm install 

# to serve with hot reload at localhost:3000
$ npm run dev

# to build for production and launch server
$ npm run build
$ npm run start 
```

## Contact

For any further questions you can reach me with mail:  volkanakks@gmail.com
Thank you! 