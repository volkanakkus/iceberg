export default function ({ $axios, $config, store, redirect }) {
  $axios.setHeader('authorization', `Bearer ${$config.API_KEY}`)

  $axios.onRequest((config) => {})

  $axios.onError((error) => {
    return error
  })

  $axios.onResponse((res) => {
    return res
  })
}
