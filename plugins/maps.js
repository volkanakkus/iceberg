import Vue from 'vue'
import * as VueGoogleMaps from 'vue2-google-maps'

export default ({ $config: { MAPS_KEY } }) => {
  Vue.use(VueGoogleMaps, {
    load: {
      key: MAPS_KEY,
      language: 'en',
      libraries: 'places',
    },
    installComponents: true,
  })
}
