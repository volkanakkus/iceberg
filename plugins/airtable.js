import Airtable from 'airtable'

export default ({ app, $config }, inject) => {
  const base = new Airtable({ apiKey: $config.API_KEY }).base($config.APP_ID)

  inject('base', base)
}
