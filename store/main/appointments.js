export const state = () => ({
  appointments: {},
})

export const getters = {
  getAppointments: (state) => state.appointments,
}

export const mutations = {
  setAppointments(state, payload) {
    state.appointments = payload
  },
}

export const actions = {
  async fetchAppointments({ commit }) {
    await this.$base('Appointments')
      .select({
        view: 'Grid view',
      })
      .eachPage(
        function page(records, fetchNextPage) {
          // BUG I'm doing this for misdirection on api
          const idFix = []

          for (let i = 0; i < records.length; i++) {
            const e = records[i]
            e.fields.id = e.id
            idFix.push(e)
          }

          const data = idFix.map((record) => (record = record.fields))

          const filteredData = data.filter(
            (record) => record.appointment_postcode !== undefined
          )

          commit('setAppointments', filteredData)

          fetchNextPage()
        },
        function done(err) {
          if (err) {
            console.error(err)
            return err
          }
        }
      )
  },
  async createAppointment({ commit }, payload) {
    await this.$base('Appointments').create(
      {
        appointment_date: payload.appointment_date,
        appointment_postcode: payload.appointment_postcode,
        contact_id: payload.contact_id,
        agent_id: payload.agent_id,
      },

      function (err, record) {
        if (err) {
          console.error(err)
          alert('Appointment Couldt Created')

          return
        }
        console.log(record.getId())
      }
    )
  },
  async updateAppointment({ dispatch }, payload) {
    await this.$base('Appointments').update(
      payload.id,
      {
        appointment_date: payload.appointment_date,
        appointment_postcode: payload.appointment_postcode,
        contact_id: payload.contact_id,
        agent_id: payload.agent_id,
      },
      function (err, record) {
        if (err) {
          console.error(err)

          alert('Appointment Couldt Updated')
        } else {
          dispatch('fetchAppointments')
          alert('Appointment Updated')
        }
      }
    )
  },
  async deleteAppointment({ dispatch }, id) {
    await this.$base('Appointments').destroy(id, function (err, deletedRecord) {
      if (err) {
        console.error(err)
        return
      }
      dispatch('fetchAppointments')

      alert('Appointment Deleted')
    })
  },
}
