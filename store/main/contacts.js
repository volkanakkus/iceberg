export const state = () => ({
  contacts: {},
  selectedContact: {},
})

export const getters = {
  getContacts: (state) => state.contacts,
  getSelectedContact: (state) => state.selectedContact,
}

export const mutations = {
  setContacts(state, payload) {
    state.contacts = payload
  },
  setSelectedContact(state, payload) {
    state.selectedContact = payload
  },
}

export const actions = {
  async fetchContacts({ commit }) {
    await this.$base('Contacts')
      .select({
        view: 'Grid view',
      })
      .eachPage(
        function page(records, fetchNextPage) {
          // BUG I'm doing this for misdirection on api
          const idFix = []

          for (let i = 0; i < records.length; i++) {
            const e = records[i]
            e.fields.id = e.id
            idFix.push(e)
          }

          const data = idFix.map((record) => (record = record.fields))

          const filteredData = data.filter(
            (record) => record.contact_name !== undefined
          )

          commit('setContacts', filteredData)

          fetchNextPage()
        },
        function done(err) {
          if (err) {
            console.error(err)
            return err
          }
        }
      )
  },
  fetchSingleContact({ commit, getters }, payload) {
    const contacts = getters.getContacts
    const searchContacts = JSON.parse(JSON.stringify(contacts))

    const selectedContact = searchContacts.find((i) => i.contact_id === payload)

    commit('setSelectedContact', selectedContact)
  },
}
