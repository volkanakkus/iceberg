export const state = () => ({
  postcode: 'CM27PJ',
  localAddress: null,
  selectedLocation: null,
  selectedPostcode: null,
})

export const getters = {
  getLocalAddress: (state) => state.localAddress,
  getSelectedLocation: (state) => state.selectedLocation,
  getSelectedPostcode: (state) => state.selectedPostcode,
}

export const mutations = {
  setLocalAddress(state, payload) {
    state.localAddress = payload
  },
  setSelectedLocation(state, payload) {
    state.selectedLocation = payload
  },
  setSelectedPostcode(state, payload) {
    state.selectedPostcode = payload
  },
}

export const actions = {
  async fetchLocalAddress({ commit, state }) {
    try {
      const data = await this.$axios.$get('/pc/postcodes/' + state.postcode)
      commit('setLocalAddress', data.result)
    } catch (err) {
      console.log(err)
    }
  },
  async searchPostcode({ commit }, payload) {
    try {
      const data = await this.$axios.$get('/pc/postcodes/' + payload)
      commit('setSelectedLocation', data.result)
    } catch (err) {
      commit('setSelectedLocation', null)
    }
  },
  async searchLocation({ commit }, payload) {
    try {
      const data = await this.$axios.$get(
        '/pc/postcodes?lon=' + payload.lng + '&lat=' + payload.lat
      )
      commit('setSelectedPostcode', data.result[0].postcode.replace(/ /g, ''))
    } catch (err) {
      commit('setSelectedPostcode', null)
    }
  },
}
