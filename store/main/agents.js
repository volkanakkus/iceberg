export const state = () => ({
  agents: {},
  selectedAgent: {},
})

export const getters = {
  getAgents: (state) => state.agents,
  getSelectedAgent: (state) => state.selectedAgent,
}

export const mutations = {
  setAgents(state, payload) {
    state.agents = payload
  },
  setSelectedAgent(state, payload) {
    state.selectedAgent = payload
  },
}

export const actions = {
  async fetchAgents({ commit }) {
    await this.$base('Agents')
      .select({
        view: 'Grid view',
      })
      .eachPage(
        function page(records, fetchNextPage) {
          // BUG I'm doing this for misdirection on api
          const idFix = []

          for (let i = 0; i < records.length; i++) {
            const e = records[i]
            e.fields.id = e.id
            idFix.push(e)
          }

          const data = idFix.map((record) => (record = record.fields))

          const filteredData = data.filter(
            (record) => record.agent_name !== undefined
          )

          commit('setAgents', filteredData)

          fetchNextPage()
        },
        function done(err) {
          if (err) {
            console.error(err)
            return err
          }
        }
      )
  },
  fetchSingleAgent({ commit, getters }, payload) {
    const agents = getters.getAgents
    const searchAgents = JSON.parse(JSON.stringify(agents))

    const selectedAgent = searchAgents.find((i) => i.agent_id === payload)

    commit('setSelectedAgent', selectedAgent)
  },
}
