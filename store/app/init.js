export const actions = {
  initFetch({ dispatch }) {
    dispatch('main/appointments/fetchAppointments', '', { root: true })
    dispatch('main/contacts/fetchContacts', '', { root: true })
    dispatch('main/agents/fetchAgents', '', { root: true })
    dispatch('main/postcode/fetchLocalAddress', '', { root: true })
  },
}
